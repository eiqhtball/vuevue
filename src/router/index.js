import Vue from 'vue'
import Router from 'vue-router'
import appHomePage from '@/components/appHomePage'
import appAboutPage from '@/components/appAboutPage'
import appContactPage from '@/components/appContactPage'
import appProfilePage from '@/components/appProfilePage'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/home',
      name: 'appHomePage',
      component: appHomePage
    },
    {
      path: '/about',
      name: 'appAboutPage',
      component: appAboutPage
    },
    {
      path: '/contact',
      name: 'appContactPage',
      component: appContactPage
    },
    {
      path: '/profile',
      name: 'appProfilePage',
      component: appProfilePage
    }
  ]
})
